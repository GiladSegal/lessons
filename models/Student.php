<?php

namespace app\models;

use yii\db\ActiveRecord;

class Student extends ActiveRecord
{

	public static function tableName(){
		return 'student';
	
	}
	
	public static function getName($id){
		$student = self::findOne($id);
		isset($student)?
		$return = $student->name:
		$return = "No Student found with id $id";
		return $return;
	}








#    private static $students = [
#        '1' => [
#            'name' => 'Jack',
#        ],
#        '2' => [
#            'name' => 'John',
#        ],
#    ];
	
	#public static function getName($id){
		//simmilar to if
	#	isset(self::$students[$id]['name'])?
		//true
	#	$return = self::$students[$id]['name']:
		//false
	#	$return = "No Student found with id $id";
	#	return $return;
	
	
#	}	
}

