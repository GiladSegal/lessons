<?php

use yii\db\Migration;

class m170515_135547_creat_student_table extends Migration
{
    public function up()
    {
		$this->createTable('student', [
			'id'=>$this->primaryKey(),
			  'name' => $this->string()->notNull(),
		]);
    }

    public function down()
    {
       
	$this->dropTable('student');

    }

    
}
