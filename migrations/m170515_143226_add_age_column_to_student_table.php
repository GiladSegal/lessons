<?php

use yii\db\Migration;

/**
 * Handles adding age to table `student`.
 */
class m170515_143226_add_age_column_to_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('student', 'age', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('student', 'age');
    }
}
